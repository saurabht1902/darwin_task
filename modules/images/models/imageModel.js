//var _ = require('lodash');
var mongoose = require('mongoose');
var Schema = require('mongoose').Schema;
// var plugins = require('../../../plugins');
// var autopopulate = require('mongoose-autopopulate');

// transform
var omitPrivate = function (doc, activity) {
    return activity;
};

// options
var options = {
    toJSON: { virtuals: true, transform: omitPrivate },
    toObject: { virtuals: true, transform: omitPrivate }
};

// schema
var schema = new Schema({
	searchText : {type: String},
	images : {type: String}

}, options);

// plugins
// schema.plugin(plugins.mongooseFindPaginate);
// schema.plugin(plugins.mongooseSearch, ['moduleType','actionPerformed']);

// // autopopulate plugin
// schema.plugin(autopopulate);

module.exports = mongoose.model('activityLog', schema);