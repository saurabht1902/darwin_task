var imageModel = require('./models/imageModel');

var imageModule = {};
module.exports = imageModule;

// upload images
imageModule.uploadImages = function (data) {
    return new imageModel(data).save();
};

//get list of searched items
imageModule.gatList = function () {
    return imageModel.find().exec();
};

//get images of selected item
imageModule.showImage = function (seartext) {
    console.log("hello user");
    return imageModel.find({"searchText":seartext}).exec();
};