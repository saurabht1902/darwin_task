var Route = require('../../lib/Route');
var errors = require('../../lib/errors');
var imageModule = require('../../modules/images');
var Scraper = require ('images-scraper')
  , google = new Scraper.Google();


var route = new Route('post', '/getimage');
module.exports = route;

// allow any user
route.setPublic();


//console.log(req.body.keyword);
route.use(function (req, res, next) {
    google.list({
    keyword: req.body.keyword,
    num: 10,
    detail: true,
    nightmare: {
        show: true
    }
})
.then(function (res) {
    return stallModule.uploadImages(res)
        .then(function (doc) {
    console.log('first 10 results from google', res);
    res.json("images saved");
    })
        .catch(next);
}).catch(function(err) {
    console.log('err', err);
});
 
// you can also watch on events 
// google.on('result', function (item) {
//     console.log('out', item);
// });
})
